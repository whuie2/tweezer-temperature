use std::path::PathBuf;
use ndarray::{ self as nd, array };
use whooie::{ loop_call, mkdir, write_npz };
use lib::rampdown::trap::{ Trap1D, Gaussian1D, rampdown_survival };

const DEPTH: f64 = 580.0; // μK
//const WAIST: f64 = 1.100; // μm
const WAIST: f64 = 5.572; // μm

fn main() -> anyhow::Result<()> {
    let outdir = PathBuf::from("output");
    if !outdir.is_dir() { mkdir!(outdir); }
    let outfile = outdir.join("rampdown_curves.npz");

    let temp: nd::Array1<f64>
        = array![20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0]; // μK
    let energy: nd::Array1<f64>
        = nd::Array1::linspace(DEPTH * 1e-2, DEPTH, 200); // μK
    let trap = Gaussian1D::new(WAIST);

    let mut caller = |ijk:Vec<usize>| -> (f64, f64) {
        let u: f64 = trap.find_escape_depth(energy[ijk[1]], DEPTH).unwrap();
        let s: f64 = rampdown_survival(energy[ijk[1]], temp[ijk[0]]);
        (u, s)
    };
    let (uesc, surv): (nd::ArrayD<f64>, nd::ArrayD<f64>)
        = loop_call!(
            caller => ( u: f64, s: f64 ),
            vars: { temp, energy },
        );

    write_npz!(
        outfile,
        arrays: {
            "temp" => &temp,
            "energy" => &energy,
            "uesc" => &uesc,
            "surv" => &surv,
            "consts" => &array![DEPTH, WAIST],
        }
    );

    Ok(())
}
