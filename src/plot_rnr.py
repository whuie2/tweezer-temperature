from pathlib import Path
import numpy as np
try:
    import libscratch.pyplotdefs as pd
except ModuleNotFoundError:
    import whooie.pyplotdefs as pd
import matplotlib.colors as mplcolors
site_colors = mplcolors.LinearSegmentedColormap.from_list(
    "vibrant",
    [
        (0.000, "#012d5e"),
        (0.125, "#0039a7"),
        (0.250, "#1647cf"),
        (0.375, "#6646ff"),
        (0.500, "#bc27ff"),
        (0.600, "#dc47af"),
        (0.800, "#f57548"),
        (0.900, "#f19e00"),
        (0.950, "#fbb800"),
        (1.000, "#fec800"),
    ],
)

infile = (
    Path("/home/coveylab/Documents/Data/atomic_data")
    .joinpath("20231016")
    .joinpath("rnr_022")
    .joinpath("survival_data.npz")
)
#infile = None
exp_data = np.load(str(infile)) if infile is not None else None
tau_tof = exp_data["tau_tof"] if infile is not None else None
surv = exp_data["sliced"] if infile is not None else None
surv_err = exp_data["sliced_err"] if infile is not None else None
N_twzr = surv.shape[0] if surv is not None else 1
N_norm = 3

sim_data = np.load("../output/rnr_curves.npz")
temp_sim = sim_data["temp"]
tof_sim = sim_data["tof"]
surv_sim = sim_data["surv"]

outdir = Path("../output/rnr")
if not outdir.is_dir():
    print(f":: mkdir -p {outdir}")
    outdir.mkdir(parents=True)

for k in range(N_twzr):
    P = (
        pd.Plotter()
        .plot(
            [], [],
            marker="", linestyle="", color="k",
            label="Temperature [$\\mu$K]",
        )
    )
    for (j, (T, s)) in enumerate(zip(temp_sim, surv_sim)):
        bw = 0.5 * j / (temp_sim.shape[0] - 1)
        P.plot(
            tof_sim, s,
            marker="", linestyle="-", color=str(bw),
            label=f"{T:.1f}",
        )
    if surv is not None:
        site_color = site_colors(k / (N_twzr - 1)) if N_twzr > 1 else "C1"
        norm = surv[k, :N_norm].mean()
        P.errorbar(
            tau_tof, surv[k, :] / norm, surv_err[k, :] / norm,
            marker="o", linestyle="", color=site_color,
        )
    (
        P
        .ggrid()
        .legend(loc="upper right", fontsize="xx-small")
        .set_ylim(-0.05, 1.05)
        .set_xlabel("Release time [$\\mu$s]")
        .set_ylabel("Survival prob.")
        .set_title(f"Site {k}")
        .savefig(outdir.joinpath(f"rnr_{k:02}.png"))
        .close()
    )

