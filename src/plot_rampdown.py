from pathlib import Path
import numpy as np
try:
    import libscratch.pyplotdefs as pd
except ModuleNotFoundError:
    import whooie.pyplotdefs as pd
import matplotlib.colors as mplcolors
site_colors = mplcolors.LinearSegmentedColormap.from_list(
    "vibrant",
    [
        (0.000, "#012d5e"),
        (0.125, "#0039a7"),
        (0.250, "#1647cf"),
        (0.375, "#6646ff"),
        (0.500, "#bc27ff"),
        (0.600, "#dc47af"),
        (0.800, "#f57548"),
        (0.900, "#f19e00"),
        (0.950, "#fbb800"),
        (1.000, "#fec800"),
    ],
)

infile = (
    Path("/home/coveylab/Documents/Data/atomic_data")
    .joinpath("20231016")
    .joinpath("rampdown_010")
    .joinpath("survival_data.npz")
)
#infile = None
exp_data = np.load(str(infile)) if infile is not None else None
u_rampdown = exp_data["U_rampdown"] if infile is not None else None
surv = exp_data["sliced"] if infile is not None else None
surv_err = exp_data["sliced_err"] if infile is not None else None
N_twzr = surv.shape[0] if surv is not None else 1
N_norm = 3

sim_data = np.load("../output/rampdown_curves.npz")
temp_sim = sim_data["temp"]
uesc_sim = sim_data["uesc"]
surv_sim = sim_data["surv"]
[init_depth, _] = sim_data["consts"]

outdir = Path("../output/rampdown")
if not outdir.is_dir():
    print(f":: mkdir -p {outdir}")
    outdir.mkdir(parents=True)

for k in range(N_twzr):
    P = (
        pd.Plotter()
        .plot(
            [], [],
            marker="", linestyle="", color="k",
            label="Temperature [$\\mu$K]",
        )
    )
    for (j, (T, u, s)) in enumerate(zip(temp_sim, uesc_sim, surv_sim)):
        bw = 0.5 * j / (temp_sim.shape[0] - 1)
        P.semilogx(
            u / init_depth, s / s.max(),
            marker="", linestyle="-", color=str(bw),
            label=f"{T:.1f}",
        )
    if surv is not None:
        site_color = site_colors(k / (N_twzr - 1)) if N_twzr > 1 else "C1"
        norm = surv[k, -N_norm:].mean()
        U0 = u_rampdown.max()
        P.errorbar(
            u_rampdown / U0, surv[k, :] / norm, surv_err[k, :] / norm,
            marker="o", linestyle="", color=site_color,
        )
    (
        P
        .ggrid()
        .legend(loc="upper left", fontsize="xx-small")
        .set_ylim(-0.05, 1.05)
        .set_xlabel("$U / U_0$")
        .set_ylabel("Survival prob.")
        .set_title(f"Site {k}")
        .savefig(outdir.joinpath(f"rampdown_{k:02}.png"))
        .close()
    )

