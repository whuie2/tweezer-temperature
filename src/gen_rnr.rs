use std::path::PathBuf;
use rand::prelude as rnd;
#[allow(unused_imports)]
use ndarray::{ self as nd, array };
use whooie::{ loop_call, mkdir, write_npz };
use lib::rnr::tweezer::Tweezer;

const N: u64 = 100000; // number of Monte Carlo draws
const MASS: f64 = 2.8384644058191793e-25; // mass of 171Yb [kg]
//const DIAM: f64 = 2.0 * 670.0e-3; // 1/e^2 diam; ~160 kHz trap freq @ 580 μK [μm]
const DIAM: f64 = 2.0 * 1.161; // 1/e^2 diam; ~50 kHz trap freq @ 580 μK [μm]
const LAMBDA: f64 = 760.0; // wavelength of the tweezer [nm]
const DEPTH: f64 = 580.0; // maximum depth of the tweezer [μK]

fn main() {
    let outdir = PathBuf::from("output");
    if !outdir.is_dir() { mkdir!(outdir); }
    let outfile = outdir.join("rnr_curves.npz");

    let temp: nd::Array1<f64>
        = array![20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0]; // μK
    let tof: nd::Array1<f64> = nd::Array1::linspace(0.0, 100.0, 101); // μs
    let tweezer = Tweezer::new(DIAM, LAMBDA, DEPTH, MASS);

    let mut rng = rnd::thread_rng();
    let mut caller = |ijk: Vec<usize>| -> (f64,) {
        (tweezer.do_recapture(temp[ijk[0]], tof[ijk[1]], N, &mut rng),)
    };
    let (surv,): (nd::ArrayD<f64>,)
        = loop_call!(
            caller => ( s: f64 ),
            vars: { temp, tof },
        );
    let surv_err = surv.mapv(|s| (s * (1.0 - s) / N as f64).sqrt());

    write_npz!(
        outfile,
        arrays: {
            "temp" => &temp,
            "tof" => &tof,
            "surv" => &surv,
            "surv_err" => &surv_err,
            "consts" => &array![
                N as f64,
                MASS,
                DIAM,
                LAMBDA,
                DEPTH,
            ],
        }
    );
}
