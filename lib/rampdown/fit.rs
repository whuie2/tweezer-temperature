use rmpfit::{
    MPFitter,
    MPPar,
    MPConfig,
    MPStatus,
    MPSuccess,
    MPResult,
    MPError,
};
use thiserror::Error;
use whooie::utils::ExpVal;
use crate::rampdown::trap::{ Trap1D, TrapError, rampdown_survival };

#[derive(Debug, Error)]
pub enum FitError {
    #[error("data arrays must have equal lengths\nx: {0}\ny: {1}\n err: {2}")]
    DataLen(usize, usize, usize),

    #[error("trap error: {0}")]
    TrapError(#[from] TrapError),

    #[error("error while fitting: {0}")]
    MPError(String),

    #[error("error in fit endpoint: {0:?}")]
    Endpoint(FitEndpointError),
}
pub type FitResult<T> = Result<T, FitError>;

#[derive(Copy, Clone, Debug)]
pub enum FitEndpointError {
    /// Incomplete fit.
    Incomplete,

    /// Maximum number of iterations reached.
    MaxIter,

    /// chi^2 tolerance too small.
    Chi2TolTooSmall,

    /// Param tolerance too small.
    ParamTolTooSmall,

    /// Orthogonality tolerance too small.
    OrthoTolTooSmall,
}

/// Fits a rampdown survival curve to a set of data to determine the temperature
/// of the atomic population in the trap.
#[derive(Clone, Debug)]
pub struct TemperatureFit<T>
where T: Trap1D
{
    trap: T,
    init_depth: f64,
    data: Vec<(f64, f64, f64)>, // (rampdown depth, survival, survival error)
}

impl<T> TemperatureFit<T>
where T: Trap1D
{
    pub fn new<'u, 's, 'e, U, S, E>(
        trap: T,
        init_depth: f64,
        rampdown_depth: U,
        survival: S,
        survival_err: E,
    ) -> FitResult<Self>
    where
        U: IntoIterator<Item = &'u f64>,
        S: IntoIterator<Item = &'s f64>,
        E: IntoIterator<Item = &'e f64>,
    {
        let depth: Vec<f64> = rampdown_depth.into_iter().copied().collect();
        let surv: Vec<f64> = survival.into_iter().copied().collect();
        let surv_err: Vec<f64> = survival_err.into_iter().copied().collect();
        if
            depth.len() != surv.len()
                || depth.len() != surv_err.len()
                || surv.len() != surv_err.len()
        {
            return Err(
                FitError::DataLen(depth.len(), surv.len(), surv_err.len()));
        }
        let data: Vec<(f64, f64, f64)>
            = depth.into_iter().zip(surv).zip(surv_err)
            .map(|((x, y), e)| (x, y, e))
            .collect();
        Ok(Self { trap, init_depth, data })
    }

    pub fn do_fit(&self, T_guess: f64) -> FitResult<ExpVal> {
        let mut params: [f64; 1] = [T_guess];
        let fit_result: MPStatus
            = self.mpfit(
                &mut params,
                Some(&[
                    MPPar {
                        fixed: false,
                        limited_low: true,
                        limited_up: true,
                        limit_low: 0.0,
                        limit_up: self.init_depth,
                        ..MPPar::default()
                    }
                ]),
                &MPConfig::default(),
            )
            .map_err(|e| FitError::MPError(e.to_string()))?;
        match fit_result.success {
            MPSuccess::Chi
                | MPSuccess::Par
                | MPSuccess::Both
                | MPSuccess::Dir
                => Ok(()),
            MPSuccess::NotDone
                => Err(FitError::Endpoint(FitEndpointError::Incomplete)),
            MPSuccess::MaxIter
                => Err(FitError::Endpoint(FitEndpointError::MaxIter)),
            MPSuccess::Ftol
                => Err(FitError::Endpoint(FitEndpointError::Chi2TolTooSmall)),
            MPSuccess::Xtol
                => Err(FitError::Endpoint(FitEndpointError::ParamTolTooSmall)),
            MPSuccess::Gtol
                => Err(FitError::Endpoint(FitEndpointError::OrthoTolTooSmall)),
        }?;
        Ok(ExpVal::new(params[0], fit_result.xerror[0]))
    }
}

impl<T> MPFitter for TemperatureFit<T>
where T: Trap1D
{
    fn eval(&self, params: &[f64], deviates: &mut [f64]) -> MPResult<()> {
        let temp = params[0];
        let mut energy: f64;
        let mut surv: f64;
        for (dev, (x, y, err)) in deviates.iter_mut().zip(&self.data) {
            energy
                = self.trap.find_energy_for_depth(*x, self.init_depth)
                .map_err(|err| {
                    println!("{}", err);
                    MPError::Eval
                })?;
            surv = rampdown_survival(energy, temp);
            *dev = ((surv - *y) / *err).powi(2);
        }
        Ok(())
    }

    fn number_of_points(&self) -> usize { self.data.len() }
}

