use ndarray as nd;
use thiserror::Error;
use crate::math::{ romberg_integrate, secant_search_res, MathError };

#[derive(Debug, Error)]
pub enum TrapError {
    #[error("math error: {0}")]
    MathError(#[from] MathError),
}
pub type TrapResult<T> = Result<T, TrapError>;

/// Describes a one-dimensional trapping potential with methods to calculate
/// the rampdown-invariant action.
pub trait Trap1D {
    /// Some characteristic trap length.
    fn size(&self) -> f64;

    /// Trap potential for a given maximum depth.
    fn potential(&self, x: f64, depth: f64) -> f64;

    /// Classical turning point of an atom in the trap.
    fn turning_point(&self, energy: f64, depth: f64) -> f64;

    /// Integrand of the action-like invariant of the rampdown process.
    ///
    /// See also [`Self::action`].
    fn action_integrand(&self, x: f64, energy: f64, depth: f64) -> f64 {
        let E: f64 = energy - depth;
        let V: f64 = self.potential(x, depth);
        (E - V).max(0.0).sqrt()
    }

    /// Evaluate the "action" of an atom with a certain energy for a given trap
    /// depth.
    ///
    /// The "action" here is the integral over space of the square root of the
    /// Lagrangian and is invariant in a fully adiabatice rampdown process.
    fn action(&self, energy: f64, depth: f64) -> TrapResult<f64> {
        let outer: f64
            = if energy < depth {
                self.turning_point(energy, depth)
            } else {
                10.0 * self.size()
            };
        let action
            = romberg_integrate(
                |x| self.action_integrand(x, energy, depth),
                0.0..outer,
                None,
                None,
            )?;
        Ok(action)
    }

    /// Find the rampdown trap depth at which atoms of energy `energy` will
    /// necessarily be ejected from the trap for initial trap depth `depth`.
    fn find_escape_depth(&self, energy: f64, init_depth: f64)
        -> TrapResult<f64>
    {
        let target_action = self.action(energy, init_depth)?;
        let depth
            = secant_search_res(
                |u| self.action(u, u).map(|s| s - target_action),
                1e-6 * init_depth..init_depth,
                None,
                None,
            )?;
        Ok(depth)
    }

    /// Invert [`Self::find_escape_depth`].
    fn find_energy_for_depth(&self, escape_depth: f64, init_depth: f64)
        -> TrapResult<f64>
    {
        let energy
            = secant_search_res(
                |e| {
                    self.find_escape_depth(e, init_depth)
                        .map(|u| u - escape_depth)
                },
                5e-2 * init_depth..init_depth,
                None,
                None,
            )?;
        Ok(energy)
    }

    /// Calculate a full rampdown survival curve, given a certain atomic
    /// temperature and initial trap depth.
    fn gen_survival<'e, E>(&self, energies: E, temp: f64, init_depth: f64)
        -> TrapResult<(nd::Array1<f64>, nd::Array1<f64>)>
    where E: IntoIterator<Item = &'e f64>
    {
        let energies: Vec<f64> = energies.into_iter().copied().collect();
        let depths: nd::Array1<f64>
            = energies.iter().copied()
            .map(|e| self.find_escape_depth(e, init_depth))
            .collect::<TrapResult<nd::Array1<f64>>>()?;
        let survival: nd::Array1<f64>
            = energies.iter().copied()
            .map(|e| rampdown_survival(e, temp))
            .collect();
        Ok((depths, survival))
    }
}

/// 1D trap with a Gaussian potential.
///
/// All lengths are in μm and all energies are in μK.
#[derive(Copy, Clone, Debug)]
pub struct Gaussian1D {
    waist: f64,
}

impl Gaussian1D {
    /// Create a new 1D Gaussian trap.
    ///
    /// **Units:**
    /// - `waist`: μm
    pub fn new(waist: f64) -> Self { Self { waist: waist.abs() } }
}

impl Trap1D for Gaussian1D {
    fn size(&self) -> f64 { self.waist }

    fn potential(&self, x: f64, depth: f64) -> f64 {
        -depth * (-2.0 * (x / self.waist).powi(2)).exp()
    }

    fn turning_point(&self, energy: f64, depth: f64) -> f64 {
        self.waist * (-0.5 * (1.0 - energy / depth).ln()).sqrt()
    }
}

/// Calculate the probability that an atom from an initial population of
/// temperature `temp` has energy in the trap less than or equal to `energy`.
pub fn rampdown_survival(energy: f64, temp: f64) -> f64 {
    let eta = energy / temp;
    1.0 - (-eta).exp() * (1.0 + eta + 0.5 * eta.powi(2))
}

