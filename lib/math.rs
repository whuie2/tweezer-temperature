use std::collections::HashMap;
use ndarray as nd;
use whooie::math::integrate::trapz;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum MathError {
    #[error("romberg_integrate: failed to converge")]
    RombergConverge,

    #[error("secant_search: bounds must have start < end")]
    SecantSearchBounds,

    #[error("secant_search: bracket endpoints must have opposite sign")]
    SecantSearchBracket,

    #[error("secant_search_res: error in function eval: {0}")]
    SecantSearchEval(String),

    #[error("secant_search: failed to converge")]
    SecantSearchConverge,
}
pub type MathResult<T> = Result<T, MathError>;

/// Perform Romberg integration to calculate the definite integral of `f`.
pub fn romberg_integrate<F>(
    f: F,
    bounds: std::ops::Range<f64>,
    n_max: Option<u32>,
    epsilon: Option<f64>,
) -> MathResult<f64>
where F: Fn(f64) -> f64
{
    let mut terms: HashMap<(u32, u32), f64> = HashMap::new();
    let n_max = n_max.unwrap_or(31);
    let epsilon = epsilon.unwrap_or(1e-6).abs();
    let mut x_vals: nd::Array1<f64>;
    let mut dx: f64;
    let mut y_vals: nd::Array1<f64>;
    let mut integ: f64;
    let mut extrap: f64;
    let mut four_m: f64;
    let mut extrap_diff: f64;
    for n in 0..=n_max {
        x_vals = nd::Array1::linspace(
            bounds.start, bounds.end, 2_usize.pow(n) + 1);
        dx = x_vals[1] - x_vals[0];
        y_vals = x_vals.mapv(&f);
        integ = trapz(&y_vals, &dx);
        terms.insert((n, 0), integ);
        for m in 1..=n {
            four_m = 4.0_f64.powi(m as i32);
            extrap
                = (four_m - 1.0).recip() * (
                    four_m * *terms.get(&(n, m - 1)).unwrap()
                    - *terms.get(&(n - 1, m - 1)).unwrap()
                );
            terms.insert((n, m), extrap);
        }
        if n > 0 {
            extrap_diff
                = *terms.get(&(n, n)).unwrap()
                - *terms.get(&(n, n - 1)).unwrap();
            if extrap_diff.abs() < epsilon {
                return Ok(*terms.get(&(n, n)).unwrap());
            }
        }
    }
    Err(MathError::RombergConverge)
}

/// Use the secant method to find a root of `f` within the given bounds.
///
/// The bounds provided must have `start` < `end` and the value of the function
/// must have opposite signs at the bounds provided.
pub fn secant_search<F>(
    f: F,
    bounds: std::ops::Range<f64>,
    max_iter: Option<u32>,
    epsilon: Option<f64>,
) -> MathResult<f64>
where F: Fn(f64) -> f64
{
    let mut xl: f64 = bounds.start;
    let mut xr: f64 = bounds.end;
    (xl < xr).then_some(()).ok_or(MathError::SecantSearchBounds)?;
    let mut yl: f64 = f(xl);
    let mut yr: f64 = f(xr);
    (yl * yr <= 0.0).then_some(()).ok_or(MathError::SecantSearchBracket)?;
    let max_iter = max_iter.unwrap_or(1000000);
    let epsilon = epsilon.unwrap_or(1e-6);
    let mut xm: f64;
    let mut ym: f64;
    for _ in 0..max_iter {
        xm
            = (xl - (xr - xl) / (yr - yl) * yl)
            .max(bounds.start)
            .min(bounds.end);
        ym = f(xm);
        if yl * ym < 0.0 {
            xr = xm;
            yr = ym;
        } else if yr * ym < 0.0 {
            xl = xm;
            yl = ym;
        }
        if (xr - xl).abs() < epsilon || ym.abs() < epsilon {
            return Ok(xm);
        }
    }
    Err(MathError::SecantSearchConverge)
}

/// Use the secant method to find a root of `f` within the given bounds.
///
/// The bounds provided must have `start` < `end` and the value of the function
/// must have opposite signs at the bounds provided.
pub fn secant_search_res<F, E>(
    f: F,
    bounds: std::ops::Range<f64>,
    max_iter: Option<u32>,
    epsilon: Option<f64>,
) -> MathResult<f64>
where
    F: Fn(f64) -> Result<f64, E>,
    E: std::error::Error,
{
    let mut xl: f64 = bounds.start;
    let mut xr: f64 = bounds.end;
    (xl < xr).then_some(()).ok_or(MathError::SecantSearchBounds)?;
    let mut yl: f64
        = f(xl).map_err(|e| MathError::SecantSearchEval(e.to_string()))?;
    let mut yr: f64
        = f(xr).map_err(|e| MathError::SecantSearchEval(e.to_string()))?;
    (yl * yr <= 0.0).then_some(()).ok_or(MathError::SecantSearchBracket)?;
    let max_iter = max_iter.unwrap_or(1000000);
    let epsilon = epsilon.unwrap_or(1e-6);
    let mut xm: f64;
    let mut ym: f64;
    for _ in 0..max_iter {
        xm
            = (xl - (xr - xl) / (yr - yl) * yl)
            .max(bounds.start)
            .min(bounds.end);
        ym = f(xm).map_err(|e| MathError::SecantSearchEval(e.to_string()))?;
        if yl * ym < 0.0 {
            xr = xm;
            yr = ym;
        } else if yr * ym < 0.0 {
            xl = xm;
            yl = ym;
        }
        if (xr - xl).abs() < epsilon || ym.abs() < epsilon {
            return Ok(xm);
        }
    }
    Err(MathError::SecantSearchConverge)
}

