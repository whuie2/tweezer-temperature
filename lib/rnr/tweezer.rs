use std::f64::consts::PI;
use rand::{ Rng, distributions::Distribution };
use statrs::distribution::Normal;
use crate::{
    phys::{ g, hbar, kB },
    rnr::threevector::ThreeVector,
};

/// A single tweezer trap.
#[derive(Copy, Clone, Debug)]
pub struct Tweezer {
    diam: f64,
    radius: f64,
    rayl: f64,
    wavelength: f64,
    depth: f64,
    mass: f64,
}

impl Tweezer {
    /// Create a new `Tweezer`.
    ///
    /// **Units:**
    /// - `diam`: μm
    /// - `wavelength`: nm
    /// - `depth`: μK
    /// - `mass`: kg
    pub fn new(
        diam: f64, // μm
        wavelength: f64, // nm
        depth: f64, // μK
        mass: f64, // kg
    ) -> Self {
        // convert all quantities to SI internally
        let diam = diam.abs() * 1e-6;
        let wavelength = wavelength.abs() * 1e-9;
        let depth = depth.abs() * 1e-6 * kB;
        let mass = mass.abs();
        let radius = diam / 2.0;
        let rayl = PI * radius.powi(2) / wavelength;
        Self {
            diam,
            radius,
            rayl,
            wavelength,
            depth,
            mass,
        }
    }

    /// Calculate the value of the tweezer potential at a single point. All
    /// units are SI.
    fn potential(&self, pos: ThreeVector) -> f64 {
        // radius at z
        let r_z = self.radius * (1.0 + (pos.z / self.rayl).powi(2)).sqrt();
        -self.depth
            * (self.radius / r_z).powi(2)
            * (-2.0 / r_z.powi(2) * (pos.x.powi(2) + pos.y.powi(2))).exp()
    }

    /// Sample an initial velocity from a Maxwell-Boltzmann distribution. All
    /// units are SI.
    fn sample_velocity<R>(&self, temp: f64, rng: &mut R) -> ThreeVector
    where R: Rng + ?Sized
    {
        let v_rms = (kB * temp / self.mass).sqrt();
        let dist = Normal::new(0.0, v_rms)
            .expect("error creating velocity distribution");
        ThreeVector {
            x: dist.sample(rng),
            y: dist.sample(rng),
            z: dist.sample(rng),
        }
    }

    /// Sample an initial position from a 3D Gaussian distribution whose
    /// directional scale parameters are calculated from a sum over the spatial
    /// spreads of harmonic oscillator states, weighted appropriately by a
    /// Boltzmann distribution. All units are SI.
    fn sample_position<R>(&self, temp: f64, rng: &mut R) -> ThreeVector
    where R: Rng + ?Sized
    {
        let omega_rad
            = ((4.0 * self.depth) / (self.mass * self.radius.powi(2))).sqrt();
        let sigma_rad = (
            hbar / (self.mass * omega_rad)
            + ((2.0 * hbar) / (self.mass * omega_rad))
                / (((hbar * omega_rad) / (kB * temp)).exp() - 1.0)
        ).sqrt();
        let omega_ax
            = ((4.0 * self.depth) / (self.mass * self.rayl.powi(2))).sqrt();
        let sigma_ax = (
            hbar / (self.mass * omega_ax)
            + ((2.0 * hbar) / (self.mass * omega_ax))
                / (((hbar * omega_ax) / (kB * temp)).exp() - 1.0)
        ).sqrt();

        let dist_rad = Normal::new(0.0, sigma_rad)
            .expect("error creating radial position distribution");
        let dist_ax = Normal::new(0.0, sigma_ax)
            .expect("error creating axial position distribution");
        ThreeVector {
            x: dist_rad.sample(rng),
            y: dist_rad.sample(rng),
            z: dist_ax.sample(rng),
        }
    }

    /// Calculate the recapture probability for a given temperature and
    /// time-of-flight by means of a `n`-trial Monte Carlo simulation.
    ///
    /// **Units:**
    /// - `temp`: μK
    /// - `tof`: μs
    pub fn do_recapture<R>(&self, temp: f64, tof: f64, n: u64, rng: &mut R)
        -> f64
    where R: Rng + ?Sized
    {
        let temp = temp.abs() * 1e-6;
        let tof = tof.abs() * 1e-6;
        (0..n)
            .filter(|_| {
                let v0 = self.sample_velocity(temp, rng);
                let r
                    = self.sample_position(temp, rng)
                    + v0 * tof
                    + 0.5 * ThreeVector::new(0.0, -g, 0.0) * tof.powi(2);
                let kin = 0.5 * self.mass * v0.norm().powi(2);
                let pot = self.potential(r);
                kin + pot < 0.0
            })
            .count() as f64 / n as f64
    }
}


