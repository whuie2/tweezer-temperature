//! Provides [`ThreeVector`], a simple three-element vector with arithmetic
//! operations.

use std::{
    ops::{ Neg, Add, Sub, Mul, Div },
};

/// A simple three-element vector with arithmetic operations.
#[derive(Copy, Clone, Debug)]
pub struct ThreeVector {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl ThreeVector {
    /// Create a new `ThreeVector`.
    pub fn new(x: f64, y: f64, z: f64) -> Self { Self { x, y, z } }

    /// Calculate the 2-norm of `self`.
    pub fn norm(&self) -> f64 {
        (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
    }
}

impl Neg for ThreeVector {
    type Output = Self;

    fn neg(self) -> Self { Self { x: -self.x, y: -self.y, z: -self.z } }
}

impl Add for ThreeVector {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Self { x: self.x + rhs.x, y: self.y + rhs.y, z: self.z + rhs.z }
    }
}

impl Sub for ThreeVector {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        Self { x: self.x - rhs.x, y: self.y - rhs.y, z: self.z - rhs.z }
    }
}

impl Mul<f64> for ThreeVector {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self {
        Self { x: self.x * rhs, y: self.y * rhs, z: self.z * rhs }
    }
}

impl Mul<ThreeVector> for f64 {
    type Output = ThreeVector;

    fn mul(self, rhs: ThreeVector) -> ThreeVector {
        ThreeVector { x: self * rhs.x, y: self * rhs.y, z: self * rhs.z }
    }
}

impl Div<f64> for ThreeVector {
    type Output = Self;

    fn div(self, rhs: f64) -> Self {
        Self { x: self.x / rhs, y: self.y / rhs, z: self.z / rhs }
    }
}

